var $viewport, 
    windowWidth,
    windowHeight;
;jQuery(function($) {
    windowWidth = $(window).width(),
    windowHeight = $(window).height();

  var resizeMainWindow = function(e) {
      var windowWidthNew = $(window).width();
      var windowHeightNew = $(window).height();
      if (windowWidth != windowWidthNew || windowHeight != windowHeightNew) {
          windowWidth = windowWidthNew;
          windowHeight = windowHeightNew;
      }
  };
  var cm_map, cm_markers = [], cm_markerCluster;
  var cm_infowindow = new google.maps.InfoWindow();
  var map;
  var myCentreLat = 10.7738024;
  var myCentreLng = 106.682188;
  var initialZoom = 6;

  var adaptisDatepicker = (function(window){
      var format      = 'dd/mm/yy';
      var beFormat    = 'DD/MM/YYYY';
      var lang        = 'en';

      var months      = 2;
      // Setup datepickers.

      $.datepicker.setDefaults({
        minDate:        0,
        numberOfMonths: months,
        dateFormat:     format,
      });

      // Put default date to date input fields.
      var today       = moment( new Date() )
      var tomorrow    = moment( new Date() ).add( 1, 'days' );

      var $checkinInput = $('.js_qs_checkin');
      var $checkoutInput = $('.js_qs_checkout');
      var qsUpdateSelect = function(name, date ) {
        $('.js_' + name ).val( date ).change();
        $('.js_' + name ).val( date ).change();

        date = $('.js_' + name).val();
        var dateArr = date.split("/");

        var parent = $('.js_' + name).parent();
        parent.find("option[selected=selected]").removeAttr('selected');
        if(lang  === 'en') {
            parent.find(".js_" + name + "-month").val(dateArr[1]);
            parent.find(".js_" + name + "-day").val(dateArr[0]);
            parent.find(".js_" + name + "-year").val(dateArr[2]);
        } else {
            parent.find(".js_" + name + "-month").val(dateArr[1]);
            parent.find(".js_" + name + "-day").val(dateArr[2]);
            parent.find(".js_" + name + "-year").val(dateArr[0]);
        }
      }

      var init = function() {

          qsUpdateSelect( 'checkin',  today.format(beFormat) );
          qsUpdateSelect( 'checkout', tomorrow.format(beFormat) );
          $checkinInput.val(today.format(beFormat));
          $checkoutInput.val(tomorrow.format(beFormat));

          $checkoutInput.datepicker({
              onSelect: function( date, obj ) {
                  var selected_date   = obj.selectedDay;
                  var seleceted_mon   = obj.selectedMonth;
                  var selected_year   = obj.selectedYear;
                  var next_date       = moment( new Date(selected_year, seleceted_mon, selected_date) );

                  qsUpdateSelect( 'checkout', next_date.format(beFormat) );
              }
          });

          $checkinInput.datepicker({
              onSelect: function( date, obj ) {
                  var selected_date = obj.selectedDay
                  var seleceted_mon = obj.selectedMonth
                  var selected_year = obj.selectedYear
                  var set_date      = moment( new Date(selected_year, seleceted_mon, selected_date) )
                  var next_date     = set_date.add(1, 'days').format( beFormat );

                  $checkoutInput.datepicker( 'option', { minDate: next_date });

                  qsUpdateSelect( 'checkin', set_date.subtract(1, 'day').format(beFormat) );
                  qsUpdateSelect( 'checkout', next_date );
                  $checkoutInput.val(next_date);
                  setTimeout(function() {
                      $checkoutInput.datepicker("show");
                  },16);

              }
          });
          
      };

      return {
          init: init
      };

  })(window);

  function initialize_destination_map() {

    var myOptions = {
      zoom: initialZoom,
      center: new google.maps.LatLng(myCentreLat, myCentreLng),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{"featureType": "poi.business", "elementType": "labels", "stylers": [{ "visibility": "off" }]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    google.maps.event.addListener(map, 'click', function () {
      infowindow.close();
    });

  }

  google.maps.event.addDomListener(window,'load',initialize_destination_map);

  // header scrolling
  function header_scroll() {
    // elems
    var header_wrap = $(".wrapper");
    var sshow = $(".banner__block");
    // waypoint
    var menu_waypoint;
    menu_waypoint = (sshow.length>0) ? parseInt(sshow.outerHeight(true)+80) : 0 ;

    //open button:
    if( menu_waypoint !== 0 && $(window).scrollTop() >= menu_waypoint){
      header_wrap.addClass('is_scrolling');
    } else {
      header_wrap.removeClass('is_scrolling');
    }

    var scrollDistance = $(window).scrollTop() + 300;
  
    // Assign active class to nav links while scolling
    $('.section__animated').each(function(i) {
      if($(this).position().top <= scrollDistance) {
        $(this).siblings('.section__animated').removeClass('in');  
        $(this).addClass('in');
      }
    });
  }
  $(window).bind('resize', resizeMainWindow);
  $(document).ready(function() {
    $('.js-menu-toggle').on('click',function(e){
      e.preventDefault();
      $(this).parent('.menu').toggleClass('open');
    });
    $('.js-login-btn').on('click',function(e){
      if(windowWidth >= 992) {
        e.preventDefault(); 
        $('.js-login-popup').slideToggle();
      }
    });
    $('.js-submenu').on('click',function(){
      if(windowWidth < 992) {
        $(this).toggleClass('open');
        $(this).siblings('.menu__container--submenu').slideToggle();
      }
    });
    $('[data-target]').on('click',function(e){
      e.preventDefault();
      var target = $(this).attr('data-target'),
          move = $(target).offset().top;
      $('html, body').animate({scrollTop: move}, 500);
    });

    // Select2 
    $.fn.select2.defaults.set( "theme", "bootstrap" );
    var placeholder = $('.select2-places').attr('placeholder');
    
    $('.select2-places').select2({
      placeholder: placeholder,
      width: null,
      containerCssClass: ':all:'
    });

    $('.select2-rooms').select2({
      placeholder: placeholder,
      width: null,
      minimumResultsForSearch: -1,
      containerCssClass: ':all:'
    });

    if($('.js-inspire-slider').length){
      $('.js-inspire-slider').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 5000,
        centerMode: true,
        slidesToShow: 1,
      }); 
    }
    if($('.js-feature-slider').length){
      $('.js-feature-slider').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 5000,
        centerMode: true,
        slidesToShow: 1,
      }); 
    }
    $('.js-feature-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      console.log(currentSlide);
      console.log(nextSlide);
      $('[data-panel-index='+currentSlide+']').removeClass('show');
      $('[data-panel-index='+nextSlide+']').addClass('show');
    });
    if($('.js-gallery-slider').length){
      $('.js-gallery-slider').slick({
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      }); 
    }
    if($('.js-offer-slider').length){
      $('.js-offer-slider').slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      }); 
    }

    adaptisDatepicker.init();
    header_scroll();


  });
  $(window).load(function() {
    $( '.js_preload' ).fadeOut();
  });
  $(window).scroll(function(){
    header_scroll();
  });

  

});
