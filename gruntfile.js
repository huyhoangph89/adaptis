module.exports = function(grunt) {
    'use strict';
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            views:   'source/views/',
            styles:  'source/sass/',
            imgs:    'source/images/',
            js:      'source/js/',
            public:  'public/'
        },
        sass: {
            dev: {
                files: {
                    "public/css/style.css": "source/sass/bootstrap.scss"
                }
            },
        },
        jade: {
            compile: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [{
                    cwd: "<%= meta.views %>",
                    src: "*.jade",
                    dest: "public",
                    ext: ".html",
                    expand: true,
                }]
            }
        },
        concat: {
            dist: {
                files: [{
                    // '<%= meta.public %>js/libs/modernizr.min.js': ['<%= meta.js %>libs/modernizr.3.3.1.js'],
                    '<%= meta.public %>js/libs/jquery.min.js': ['<%= meta.js %>libs/jquery.min.js'],
                    '<%= meta.public %>js/libs/bootstrap.min.js': ['<%= meta.js %>libs/bootstrap.min.js'],
                    '<%= meta.public %>js/libs/plugins.min.js': ['<%= meta.js %>/libs/plugins/*.js'],
                    '<%= meta.public %>js/main.js': ['<%= meta.js %>plugins/*.js', '<%= meta.js %>main.js']
                }]
            }
        },
        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: '<%= meta.imgs %>',
                    src: '**/*.{png,gif,jpg,sgv, pdf}',
                    dest: '<%= meta.public %>images',
                    filter: 'isFile',
                    flatten: false
                }, {
                    expand: true,
                    src: '<%= meta.styles %>*.css',
                    dest: '<%= meta.public %>css',
                    flatten: true
                }, {
                    expand: true,
                    src: 'source/fonts/*',
                    dest: '<%= meta.public %>fonts',
                    flatten: true
                }]
            }
        },
        cssmin: {
            options: {
                advanced: false,
                keepBreaks: false,
                keepSpecialComments: 0
            },
            compress: {
                files: [{
                    '<%= meta.public %>css/style.css': '<%= meta.public %>css/style.css'
                }]
            }
        },
        uglify: {
            options: {
            compress: true,
            beautify: false,
            preserveComments: false
          },
          dist: {
            files: [{
              '<%= meta.public %>js/*.js': ['<%= meta.js %>*.js']
            }]
          }
        },
        connect: {
            server: {
                options: {
                    port: 3000,
                    hostname: '*',
                    base: {
                        path: '<%= meta.public %>',
                        options: {
                            index: 'index.html',
                            maxAge: 300000
                        }
                    }
                }
            }
        },
        watch: [
            {
                files: '<%= meta.styles %>**',
                tasks: ['sass:dev']
            }, {
                files: '<%= meta.views %>**',
                tasks: ['jade']
            }, 
            {
                files: '<%= meta.imgs %>**',
                tasks: ['copy']
            }, 
            {
                files: '<%= meta.js %>/**/*.js',
                tasks: ['concat', 'copy']
            }, 
            {
                files: 'source/fonts/*',
                tasks: ['copy']
            },
            {
                files: 'source/pdf/*',
                tasks: ['copy']
            }
        ]
    });

    grunt.file.expand('./node_modules/grunt-*/tasks').forEach(grunt.loadTasks);
    grunt.registerTask('default', ['sass', 'jade', 'concat', 'copy', 'connect:server', 'watch']);
    grunt.registerTask('release', ['sass', 'jade', 'concat', 'copy', 'cssmin', 'uglify']);
};
